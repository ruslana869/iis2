package dataMining;

import java.util.List;

public class Printer {

    public static void printOneR(Data fileData, String attrVal, OneR oneR) {
        Row overcast = null;
        for (Row row : fileData.getRows()) {
            if (row.getRowData().get(0).equals(attrVal)) {
                overcast = row;
                break;
            }
        }
        System.out.println("outlook = " + attrVal + ": " + oneR.classify(overcast));
    }

    public static void printZeroR(Data fileData) {
        ZeroR zeroR = new ZeroR(fileData);
        System.out.println("------------ZeroR------------");
        System.out.println("��������� ����� ������ ��� ����: " + zeroR.getSelectedClass());
        System.out.println("���-�� ����� � ����� � FALSE: " + getByVal(fileData.getRows(), fileData.getAttributeClass(), "FALSE"));
        System.out.println("���-�� ����� � ����� � TRUE: " + getByVal(fileData.getRows(), fileData.getAttributeClass(), "TRUE") + "\n");
    }

    private static int getByVal(List<Row> rows, Attribute classAttr, String val) {
        int count = 0;
        for (Row row : rows) {
            if (row.getRowData().get(classAttr.getIndex()).equals(val)) {
                count++;
            }
        }

        return count;
    }

}
