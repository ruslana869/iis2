package dataMining;

import java.io.IOException;

public class Main {
	public static void main(String[] args) throws IOException {
		String weather = "weather.nominal.arff";

		Data fileData = new Reader().read(weather);
		String selectedClass = "windy";
		fileData.setAttributeClass(selectedClass);

		System.out.println("Файл weather.nominal.arff, атрибут типа класс - " + selectedClass);
		System.out.println("Всего строк: " + fileData.getRows().size());

		Printer.printZeroR(fileData);

		OneR oneR = new OneR(fileData);

		System.out.println("------------OneR------------");
		System.out.println("Выбранный для классификации атрибут: " + oneR.getAttributeClassifyBy().getName());
		Printer.printOneR(fileData, "overcast", oneR);
		Printer.printOneR(fileData, "sunny", oneR);
		Printer.printOneR(fileData, "rainy", oneR);
	}

}
