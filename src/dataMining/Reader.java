package dataMining;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Reader {

	/**
	 * чтение и анализ файла
	 * @param filePath
	 * @return  Data
	 * @throws IOException
	 */
	public Data read(String filePath) throws IOException {
		Data data = new Data();
		boolean wasRelation = true;
		boolean wasAttribute = false;
		boolean wasRow = false;
		int idx = 0;
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filePath)))) {
			while (true) {
				String line = reader.readLine();
				if (line == null) {
					// если в файле не встретились @relation или @attribute
					if (wasRelation || wasAttribute) {
						throw new RuntimeException("Wrong file. Not all information");
					}
					return data;
				}

				line = line.trim();
				if (line.startsWith("%") || line.isEmpty()) {
					continue;
				}
				// Arff-файл должен начинаться с @relation
				if (wasRelation && !line.startsWith("@relation")) {
					throw new RuntimeException("Wrong data format! @relation should be the first");
				}
				if (wasRelation) {
					data.setName(line.substring("@relation".length()));
					wasRelation = false;
					wasAttribute = true;
					continue;
				}

				if (wasAttribute) {
					if (line.startsWith("@data")) {
						wasAttribute = false;
						wasRow = true;
						continue;
					}
					// Следующий - @attribute
					if (!line.startsWith("@attribute")) {
						throw new RuntimeException("Wrong data format! @attribute is required");
					}

					data.getAttributes().add(getAttribute(line, idx++));
				}

				if (wasRow) {
					if (line.startsWith("@relation") || line.startsWith("@data") || line.startsWith("@attribute")) {
						throw new RuntimeException("Wrong data format!");
					}

					data.getRows().add(getRow(line));
				}
			}
		}
	}
	
	/**
	 * считывает строку из файла
	 * @param line
	 * @return Row 
	 */
	private Row getRow(String line) {
		line = line.replace(", ", ",");
		String[] strs = line.split(",");

		return new Row(Arrays.asList(strs));
	}

	private Attribute getAttribute(String line, int idx) {
		String[] strs = line.split(" ");
		if (strs.length < 3) {
			throw new RuntimeException("Неправильный формат данных");
		}

		return new Attribute(strs[1], strs[2], idx);
	}
}
