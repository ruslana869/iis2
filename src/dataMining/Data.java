package dataMining;

import java.util.ArrayList;
import java.util.List;

public class Data {
	// ��������
	private String name;
	// ������ ���������
	private List<Attribute> attributes = new ArrayList<>();
	// ������ �����
	private List<Row> rows = new ArrayList<>();
	// ����� ��������
	private Attribute attributeClass;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Attribute> getAttributes() {
		return attributes;
	}

	public List<Row> getRows() {
		return rows;
	}

	public Attribute getAttributeClass() {
		return attributeClass;
	}

	// ���������� ����� ���������
	public void setAttributeClass(String attributeClassName) {
		for (Attribute attribute : attributes) {
			if (attribute.getName().equals(attributeClassName)) {
				this.attributeClass = attribute;
				break;
			}
		}
	}
}
