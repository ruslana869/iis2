package dataMining;

import java.util.HashMap;
import java.util.Map;

public class OneR {
    private Attribute classifyBy;
    private Map<String, String> classifier = new HashMap<>();

    public OneR(Data fileData) {
        setClassifier(fileData);
    }

    private void setClassifier(Data data) {
        int minWrongCount = Integer.MAX_VALUE;
        Attribute bestAttr = null;
        Map<String, Map<String, Integer>> bestAttrFrequencies = null;

        for (Attribute attribute : data.getAttributes()) {
            if (attribute==data.getAttributeClass()) {
                continue;
            }

            int error = 0;
            Map<String, Map<String, Integer>> attrFrequencies = new HashMap<>();

            for (Row row : data.getRows()) {
                String val = row.getRowData().get(attribute.getIndex());
                Map<String, Integer> attrDistribution = attrFrequencies.getOrDefault(val, new HashMap<String, Integer>());
                Integer count = attrDistribution.getOrDefault(row.getRowData().get(data.getAttributeClass().getIndex()), 0);
                attrDistribution.put(row.getRowData().get(data.getAttributeClass().getIndex()), count + 1);
                attrFrequencies.put(val, attrDistribution);
            }


            if (error < minWrongCount) {
                minWrongCount = error;
                bestAttrFrequencies = attrFrequencies;
                bestAttr = attribute;
            }
        }

        this.classifyBy = bestAttr;
        this.classifier = fillClassifier(bestAttrFrequencies);
    }

    private Map<String, String> fillClassifier(Map<String, Map<String, Integer>> frequencies) {
        Map<String, String> result = new HashMap<>();
        for (String attributeValue : frequencies.keySet()) {
            String maxClassKey = null;
            int maxClass = 0;
            for (Map.Entry<String, Integer> vals : frequencies.get(attributeValue).entrySet()) {
                if (vals.getValue() > maxClass) {
                    maxClassKey = vals.getKey();
                    maxClass = vals.getValue();
                }
            }
            result.put(attributeValue, maxClassKey);
        }

        return result;
    }

    public Attribute getAttributeClassifyBy() {
        return classifyBy;
    }

    public String classify(Row row) {
        return classifier.get(row.getRowData().get(classifyBy.getIndex()));
    }
}
