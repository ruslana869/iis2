package dataMining;

import java.util.HashMap;
import java.util.Map;

public class ZeroR {
	// �����, �� �������� �������
	private String selectedClass;

	public ZeroR(Data fileData) {
		selectMaxClass(fileData);
	}

	/**
	 * ���������� ��������� �����
	 * 
	 * @param data
	 */
	private void selectMaxClass(Data data) {
		Map<String, Integer> freq = new HashMap<>();
		Row row;
		for (int i = 0; i < data.getRows().size(); i++) {

			// for (Row row : data.getRows()) {
			row = data.getRows().get(i);
			String dataClass = row.getRowData().get(data.getAttributeClass().getIndex());
			int classFreq = freq.getOrDefault(dataClass, 0);
			freq.put(dataClass, classFreq + 1);
		}

		int maxInt = 0;
		//������� ����� ��������� �����
		for (String key : freq.keySet()) {
			int val = freq.get(key);
			if (val > maxInt) {
				maxInt = val;
				this.selectedClass = key;
			}
		}
	}

	public String getSelectedClass() {
		return selectedClass;
	}
}
