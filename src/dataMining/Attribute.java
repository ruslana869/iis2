package dataMining;

public class Attribute {
	// название
	private String name;
	private int index;

	public Attribute(String name, String type, int index) {
		this.name = name;
		this.index = index;
	}

	public String getName() {
		return name;
	}

	public int getIndex() {
		return index;
	}
}
